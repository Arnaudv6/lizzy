# Rationale behind (non-)features
 1. embed mpv in curses (as opposed to what is done today: quitting curse wrapping):
    - **pro**: all in one screen.
    - **pro**: search and queue while playing.
    - **con**: screen refreshing would be a nightmare (1"? Asynchronous input/display?)
    - **con**: to control mpv, _lizzy_ would have to depend on one of those:  
      https://github.com/mpv-player/mpv/wiki/Scripting-language-bindings
    - **con**: asynchronous is added complexity
 2. recognise mouse events (other than mouse wheel when supported by terminal) with [curses.getmouse](https://docs.python.org/3/library/curses.html#curses.getmouse)?
    - **pro**: reordering the playlist could be nice
    - **con**: next thing: users would expect complexe selections (i.e. with `[ctrl]`, `[shift]`...)
    - **con**: implement distinction between cursor position and selection, and use more color codes for visual feedback
    - **con**: can't select text anymore (copy paste saved playlist's path)
    - **con**: a lot of burden supporting different terminals I guess
 3. piping to _lizzy_ is used for debugging only.
