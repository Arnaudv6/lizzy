#!/bin/sh

set -u # exit if encountering uninitialized variable
set -e # exit on first error

# For test environments, docker / jail / chroot are overkill and miss the point here IMO
# also they can't be run and driven from a single script, even chroot.
# Other overcomplicated solutions: 'kitty --session -', 'screen -r' and 'echo lizzy > /dev/pts/3'

# future directions / todo tests:
#     wrong backendcommand
#     mpv / youtube-dl not in $PATH...
#         verify backend_command for instance is found when defined
#             in the source?
#             in ini DEFAULT section?
#             in profile section?
#     paste text, with spaces, F2 function keys...
#             multiple drag-n-drop
#     no-header ini config file (no profile name)
# test update (-u) also
#     through ssh / screen...
#     films
#     piping in/out to lizzy
#     function-level tests? To try make them crash.
# youtubedl test removed, as feature got removed

# todo:
# on uncaught exception, lizzy will exit, and remaining key sequences might be interpreted by bash.
#   trap signals and EXIT and clear stdin
# make sure my ctrl sequences are safe to press in terminal:

# I can check playlist content: just replace mpv with lTest, and read whatever file lizzy hands me

ENTER="$(printf '\n')"
true_home="${HOME}"
lizzy_bin_path="$(command -v lizzy)" # todo: or || die
fake_home=/tmp/testLizzy/
fake_stdin="${fake_home}"/input
fake_config_dir="${fake_home}"/.config
rm -rf "${fake_home}"
mkdir -p "${fake_config_dir}"/lizzy

printf "
[DEFAULT]
printWaitDelay = 1
" > "${fake_config_dir}"/lizzy/config.ini

puppet_stdin() { printf "%s" "$1" >> "${fake_stdin}"; sleep 0.3; }
launch_puppet() {
    printf "debug\n" > "${fake_stdin}"
    export HOME="${fake_home}"  # todo replace this with env command?
    tail -f "${fake_stdin}" | "${lizzy_bin_path}" "$@" &
}
breakpoint() {
    printf "\n\033[7m%s\n[Enter] to continue, [ctrl]+[c] to quit\033[0m\n\n" "${1}"
    read -r _
}
user_dirs() {
    printf '
# comment
XDG_DOWNLOAD_DIR="%s"
XDG_MUSIC_DIR=%s
XDG_PICTURES_DIR="%s/Photos/"
' "${fake_home}" "$1" "${fake_home}" > "${fake_config_dir}"/user-dirs.dirs
}

#start:#

# no user-dirs.dirs file, no last playlist, no result to show
    breakpoint "4 seconds:
  default music_index_path
  no library => no result
"
    launch_puppet
    sleep 1.2  # pass printWaitDelay
    puppet_stdin 'b'
    sleep 4
    puppet_stdin '^Q'
    puppet_stdin '^M'
    wait  # for lizzy subprocess to close
    breakpoint "
red warning: inexistant library,
white warning: inexistant playlist
"

# inexistent music_index_path, without tailing slash, errors in music files names
    breakpoint "4 seconds:
  added trailling slash to music_index_path and press ^A"
    user_dirs "${fake_home}"/collection
    launch_puppet
    sleep 1.2
    puppet_stdin '^A'
    sleep 1.2
    puppet_stdin 'A'
    puppet_stdin '^A'
    sleep 4
    puppet_stdin '^U'
    sleep 1.5
    puppet_stdin "${ENTER}"

    mkdir -p "${fake_home}"/collection
    puppet_stdin '^U'
    sleep 0.5
    puppet_stdin "${ENTER}"

    touch "${fake_home}"/collection/file"$(printf '\t')"with\ tab
    touch "${fake_home}"/collection/file"$(printf ' with\nnew line')"
    touch "${fake_home}"/collection/file"$(printf '\twith\nboth')"
    puppet_stdin '^U'
    sleep 1.5
    puppet_stdin "${ENTER}"
    rm -f "${fake_home}"/collection/*

    touch "${fake_home}"/collection/filename\ is\ ok
    puppet_stdin '^U'
    sleep 1.5
    puppet_stdin "${ENTER}"
    rm -f "${fake_home}"/collection/*

    puppet_stdin '^Q'
    puppet_stdin '^M'
    wait # for lizzy subprocess to close

    breakpoint "4 red warnings
  1. there is no index file,
  2. music path points to inexistent folder
  3. music folder is empty
  4. no valid filename in the end
"

# finally using my library
    cp "${true_home}"/.config/lizzy/*.idx "${fake_config_dir}"/lizzy/
    breakpoint '4 seconds:\n  we have search results'
    launch_puppet
    sleep 1.2
    puppet_stdin 'b'
    sleep 4
    puppet_stdin '^Q'
    puppet_stdin '^M'
    wait # for lizzy subprocess to close
    breakpoint 'everything fine here'

# corrupted picle dump
# https://stackoverflow.com/questions/1653897/if-pickling-was-interrupted-will-unpickling-necessarily-always-fail-python
    cp "${fake_config_dir}"/lizzy/DEFAULTcollection.idx "${fake_config_dir}"/lizzy/corruptedcollection.idx
    dd if=/dev/zero of="${fake_config_dir}"/lizzy/corruptedcollection.idx bs=1 count=1 seek=100 conv=notrunc
    launch_puppet --profile=corrupted
    sleep 1.5
    puppet_stdin 'b'
    sleep 0.5
    puppet_stdin '^Q'
    puppet_stdin '^M'
    wait # for lizzy subprocess to close
    breakpoint 'corrupted pickle dump'


#profiles:#
# inexistant profile
    launch_puppet --profile=innexistant
    sleep 1.5
    puppet_stdin 'b'
    sleep 4
    puppet_stdin '^Q'
    puppet_stdin '^M'
    wait # for lizzy subprocess to close
    breakpoint 'innexistant profile\nwithout even the config.ini file'

# films profile
    breakpoint 'films profile'
    launch_puppet --profile=films
    sleep 1.5
    puppet_stdin 'b'
    sleep 4
    puppet_stdin '^Q'
    puppet_stdin '^M'
    wait # for lizzy subprocess to close
# RED MESSAGE :
# WAS NOT EXPECTING THAT...?
# error: "films" profile not found

#playlists:#
# argument playlist
    breakpoint 'given playlist argument'
    printf "# comment\\n\\n\\nni commentaire\\nni ligne vide\\nvisible\\n\\n" > /tmp/testLizzy/playlist
    launch_puppet "${fake_home}"/playlist
    sleep 1.5
    puppet_stdin 'b'
    sleep 4
    puppet_stdin '^Q'
    puppet_stdin '^M'
    wait # for lizzy subprocess to close

# argument playlist is binary file
    breakpoint 'playlist argument points a binary file'
    launch_puppet "${fake_home}"/lizzy/corruptedcollection.idx
    sleep 1.5
    puppet_stdin 'b'
    sleep 4
    puppet_stdin '^Q'
    puppet_stdin '^M'
    wait # for lizzy subprocess to close

# argument playlist is inexistent file
    breakpoint 'playlist argument point to inexistent file'
    launch_puppet "${fake_home}"/inexistent_file
    sleep 1.5
    puppet_stdin '^Q'
    puppet_stdin '^M'
    wait # for lizzy subprocess to close

exit

