#!/usr/bin/env python
# -*- coding: utf-8 -*-

import setuptools
import codecs
import os.path


def get_version():
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, 'lizzy'), 'r') as fp:
        for line in fp.readlines():
            if line.startswith('SCRIPT_REVISION'):
                delim = '"' if '"' in line else "'"
                return line.split(delim)[1]
        raise RuntimeError("Unable to find version string.")


setuptools.setup(
    name="lizzy",
    version=get_version(),
    author="Arnaudv6",
    author_email="arnaudv6@free.fr",
    description="Find and play your music from the console",
    long_description='''
lizzy will instantly find your music, (movies, media...) by file names (it ignores id3 tags).
It presents a simple and colorfull terminal interface to make a playlist, then passes it to mpv.
It does much more, please refer to homepage.
It is a single python file with no dependency.''',
    url="https://gitlab.com/Arnaudv6/lizzy",
    packages=setuptools.find_packages(),
    license='GNU General Public License v3 (GPLv3)',
    platforms=[
        'posix',
    ],
    scripts=[
        'lizzy',
    ],
    py_modules=[],
    ext_modules=[],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: POSIX",
    ],
    python_requires='>=3.6',
)
